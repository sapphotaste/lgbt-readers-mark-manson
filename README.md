**Mark Manson message for LGBT people who were reading his book**

## A Quick Note to Female and LGBT Readers

The popularity of Models has brought it to the attention of a number of people that the book was not originally conceptualized and written for.
A number of single women, as well as gay, lesbian, bi, and trans people have emailed me over the years asking if the book’s concepts apply to them.
At first, this surprised me. And then what surprised me more is that as I went through the concepts in the book, I realized that the answer was a resounding ‘yes,’ these concepts apply to everyone. Although there are a few caveats.
The core principles of the book – Non-Neediness (Chapter 1), Vulnerability
(Chapters 2-3), Unconditionality (Chapter 3), Polarization and Rejection
(Chapters 4-6), Demographics ( Chapter 7), Overcoming Shame and Anxiety
(Chapters 9-10), and Intentions (Chapter 11) – apply to all human beings,
regardless of gender, orientation, genitalia or whatever.
The parts of the book that won’t directly apply are a number of the specific
examples and implementations of these core principles. For example, in most cultures, men are expected to initiate in almost every phase of courtship, therefore, the anxieties they face (Chapters 9-10), the challenges with vulnerability they will confront (Chapter 3) and so on, will look a little bit
different than they would for a hetero woman or someone of another
orientation. As long as you keep this in mind as you move throughout the book and attempt to apply the core principles to your own dating situation, you should be fine.
Women, like men, must polarize. Gay men must work on their neediness and vulnerability as well. Lesbians must learn to look at the intentions behind their communication rather than communication itself. But because the book was initially written for a hetero male audience, the examples and specific pieces of advice given are for their situations.
A number of female readers have requested that I write a female version of
Models and that is something I might do one day. But in the meantime, this is all we’ve got.
